# ok-chat

okCHAT（赞聊），一款基于Netty构建的在线聊天APP软件！

### 技术栈

<p>
  <img src="https://img.shields.io/badge/SpringBoot-2.1.8.RELEASE-brightgreen">
  <img src="https://img.shields.io/badge/netty-4.1.38.Final-brightgreen">
  <img src="https://img.shields.io/badge/MUI-v3.7.2-brightgreen">
</p>

### APP效果预览

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/0814/214105_735eeff9_1152471.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0814/214121_9cbc34f5_1152471.jpeg"/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/0814/215302_e93956c3_1152471.jpeg"/></td>
    </tr>
</table>

### 参考资料

- http://dev.dcloud.net.cn/mui
- http://www.html5plus.org/doc/h5p.html

### 开源协议

[MIT](https://gitee.com/bobi1234/ok-chat/blob/master/LICENSE)